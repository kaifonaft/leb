/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LebInterfaces;

import DataBase.DictionaryWord;

/**
 *
 * @author Александр
 */
public interface IDataSource {
    public boolean addWord(String word, String translate);
    public DictionaryWord[] searchWordByPrefix(String prefix);
    public int multiAdd(DictionaryWord[] words);
}
