/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package consts;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.ini4j.Ini;

/**
 *
 * @author Иван
 */
public class Config {
    public static int timeShowing = 7; // seconds
    public static int periodShowing = 300; //seconds
    private static String fileName = "config.ini";
    public static void read(){
        Ini prefs;
        try {
            File file = new File(fileName);
            prefs = new Ini(file);
            timeShowing = Integer.parseInt(prefs.get("general", "TimeShowing"));
            periodShowing = Integer.parseInt(prefs.get("general", "PeriodShowing"));
            System.out.println("TimeShowing: " + timeShowing);
            System.out.println("PeriodShowing: " + periodShowing);
        } catch (IOException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
