/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataBase;

import LebInterfaces.IDataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Александр
 */
public class WordSQLiteManager implements IDataSource{
    private String default_base_name = "dictionary.db";

    public WordSQLiteManager(){
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            System.out.println("Successfull open database");
            
            
            if(!databaseAlreadyInitialized(con)){
                Statement stmt = con.createStatement();
                String encodingQuery = "PRAGMA encoding = \"UTF-8\"";
                String createTabelQuery = "CREATE TABLE `dictionary` (\n" +
                    "	`word`	TEXT NOT NULL,\n" +
                    "	`translate`	TEXT NOT NULL\n" +
                    ");";
                stmt.executeUpdate(encodingQuery);
                stmt.executeUpdate(createTabelQuery);
                stmt.close();
            }
            System.out.println("after database already initialized");
            con.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean databaseAlreadyInitialized(Connection con){
        try {
            Statement stmt = con.createStatement();
            ResultSet res = stmt.executeQuery("select * FROM sqlite_master where type = 'table' and tbl_name = 'dictionary'");
            if(res.next()){
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean addWord(String word, String translate) {
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            Statement stmt = con.createStatement();
            
            System.out.println("connnection set");
            String sql = "SELECT count(*) as count FROM `dictionary` WHERE word = '"+word+"'";
            ResultSet res = stmt.executeQuery(sql);
            if(res.next() && res.getInt("count") > 0){
                System.out.println("word already in base");
                return false;
            }
            sql = "INSERT INTO dictionary VALUES (?, ?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, word);
            pst.setString(2, translate);
            pst.executeUpdate();
            pst.close();
            System.out.println("insert success");
            
            stmt.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }

    @Override
    public DictionaryWord[] searchWordByPrefix(String prefix) {
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
//            con.setAutoCommit(false);
//            Statement stmt = con.createStatement();
//            String createTableSQL = "CREATE TABLE `dictionary` (\n" +
//                "	`word`	TEXT NOT NULL,\n" +
//                "	`translate`	TEXT NOT NULL\n" +
//                ");";
//            stmt.executeUpdate(createTableSQL);
//            stmt.close();
            
            String sql = "SELECT word, translate FROM `dictionary` WHERE word LIKE ? ORDER BY `word` LIMIT 1 ;";
            System.out.println("prefix: "+prefix);
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, prefix+"%");
            
            ResultSet res = pstmt.executeQuery();
            String word, translate;
            DictionaryWord[] words = null;
            
            if(res.next()){
                word = res.getString("word");
                translate = res.getString("translate");
                System.out.println("word: "+word);
                System.out.println("translate: "+translate);
                words = new DictionaryWord[1];
                words[0] = new DictionaryWord(word, translate);
            }else{
//                word = res.getString("word");
//                System.out.println("word: "+word);
                System.out.println("no lines in querys");
            }
            
            pstmt.close();
            con.close();
            return words;
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean editWord(String old_word, String word, String translate){
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            Statement stmt = con.createStatement();
            
            System.out.println("connnection set");
            String sql = "SELECT count(*) as count FROM `dictionary` WHERE word = '"+word+"'";
            ResultSet res = stmt.executeQuery(sql);
            if(res.getInt("count") == 0){
                System.out.println("word not exist");
                return false;
            }
            sql = "UPDATE dictionary SET word = ?, translate = ? WHERE word = ?";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, word);
            pst.setString(2, translate);
            pst.setString(3, old_word);
            pst.executeUpdate();
            pst.close();
            System.out.println("update success");
            
            stmt.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void clearBase() {
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            Statement stmt = con.createStatement();
            String sql = "DELETE FROM dictionary WHERE 1";
            stmt.executeUpdate(sql);
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showDict() {
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            Statement stmt = con.createStatement();
            String sql;
            ResultSet res;
            sql = "SELECT count(*) as count FROM dictionary";
            res = stmt.executeQuery(sql);
            if(res.next()){
                int count = res.getInt("count");
                System.out.println("Words in dictionary: "+count+". First 10 words are show.");
            }
            sql = "SELECT word, translate FROM dictionary LIMIT 10";
            res = stmt.executeQuery(sql);
            while(res.next()){
                String word, translate;
                word = res.getString("word");
                translate = res.getString("translate");
                System.out.println("word: "+word+" - "+translate);
            }
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DictionaryWord getRandomWord(){
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            Statement stmt = con.createStatement();
            String sql;
            ResultSet res;
            sql = "SELECT word, translate FROM dictionary ORDER BY RANDOM() LIMIT 1";
            res = stmt.executeQuery(sql);
            if(res.next()){
                String word, translate;
                word = res.getString("word");
                translate = res.getString("translate");
                System.out.println("word: "+word+" - "+translate);
                DictionaryWord dicWord = new DictionaryWord(word, translate);
                return dicWord;
            }
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public int multiAdd(DictionaryWord[] words){
        int count = 0;
        try {
            Connection con = null;
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+default_base_name);
            System.out.println("connnection set");
            String sqlGetWord = "SELECT count(*) as count FROM `dictionary` WHERE word = ?";
            String sqlInsertWord = "INSERT INTO dictionary VALUES (?, ?)";
            PreparedStatement pstGet  = con.prepareStatement(sqlGetWord);
            PreparedStatement pstInsert = con.prepareStatement(sqlInsertWord);
            for(DictionaryWord dictWord : words){
                String word = dictWord.getWord();
                String translate = dictWord.getTranslate();
                pstGet.setString(1, word);
                
                ResultSet res = pstGet.executeQuery();
                if(res.next() && res.getInt("count") > 0){
                    System.out.println("word <"+word+"> already in base");
                }else{
                    count++;
                    pstInsert.setString(1, word);
                    pstInsert.setString(2, translate);
                    pstInsert.executeUpdate();
                }
            }
            System.out.println("insert success");
            pstInsert.close();
            pstGet.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WordSQLiteManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }
}
